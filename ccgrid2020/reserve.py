from enoslib.api import discover_networks
from enoslib.infra.enos_g5k.provider import G5k
from enoslib.infra.enos_g5k.configuration import Configuration

from yaml import load
try:
    from yaml import CLoader as Loader
except ImportError:
    from yaml import Loader

with open("conf.yaml") as f:
    g5k_config = load(f, Loader=Loader)


g5k_job = G5k(Configuration.from_dictionnary(g5k_config))
roles, networks = g5k_job.init(force_deploy=True)
